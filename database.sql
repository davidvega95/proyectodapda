CREATE DATABASE IF NOT EXISTS gestionSuperpersonas;
use gestionSuperpersonas;
CREATE TABLE usuarios(
id          int(255) auto_increment not null,
rol        varchar(20),
nombre        varchar(255),
apellidos     varchar(255),
email       varchar(255),
password    varchar(255),
CONSTRAINT pk_usuarios PRIMARY KEY(id)
)ENGINE=InnoDb; 

CREATE TABLE `gestionsuperpersonas`.`superpersonas` (
  `Id_superpersona` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(100) NOT NULL,
  `Ciudad` VARCHAR(200) NULL,
  `Fuerza` SMALLINT NOT NULL DEFAULT 0,
  `Inteligencia` SMALLINT NOT NULL DEFAULT 0,
  `Salud` SMALLINT NOT NULL,
  `Tipo` VARCHAR(100) NULL,
  `Activo` TINYINT NOT NULL DEFAULT 1,
  `Fecha_creacion` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Fecha_modificacion` DATETIME NULL,
  `IdUsuario` INT NOT NULL,
  `Slug` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`Id_superpersona`),
  UNIQUE INDEX `nombre_UNIQUE` (`Nombre` ASC),
  INDEX `pk_usuarios_idx` (`IdUsuario` ASC),
  CONSTRAINT `pk_usuarios`
    FOREIGN KEY (`IdUsuario`)
    REFERENCES `gestionsuperpersonas`.`usuarios` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

