<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Superpersonas
 *
 * @ORM\Table(name="superpersonas", uniqueConstraints={@ORM\UniqueConstraint(name="nombre_UNIQUE", columns={"Nombre"})}, indexes={@ORM\Index(name="pk_usuarios_idx", columns={"IdUsuario"})})
 * @ORM\Entity
 */
class Superpersona {

    /**
     * @var integer
     *
     * @ORM\Column(name="Id_superpersona", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idSuperpersona;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=100, nullable=false)
     * @Assert\NotBlank(message = "Este campo no puede estar vacío")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="Ciudad", type="string", length=200, nullable=true)
     */
    private $ciudad;

    /**
     * @var integer
     *
     * @ORM\Column(name="Fuerza", type="smallint", nullable=false)
     */
    private $fuerza = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="Inteligencia", type="smallint", nullable=false)
     */
    private $inteligencia = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="Salud", type="smallint", nullable=false)
     */
    private $salud;

    /**
     * @var string
     *
     * @ORM\Column(name="Tipo", type="string", length=100, nullable=true)
     */
    private $tipo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Activo", type="boolean", nullable=false)
     */
    private $activo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha_creacion", type="datetime", nullable=false)
     */
    private $fechaCreacion = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha_modificacion", type="datetime", nullable=true)
     */
    private $fechaModificacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="IdUsuario", type="integer", nullable=false)
     */
    private $idusuario;

    /**
     * @var string
     *
     * @ORM\Column(name="Slug", type="string", length=100, nullable=false)
     */
    private $slug;

    public function __construct() {
        $this->fechaCreacion = new \DateTime();
    }

    /**
     * Get idSuperpersona
     *
     * @return integer
     */
    public function getIdSuperpersona() {
        return $this->idSuperpersona;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Superpersona
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set ciudad
     *
     * @param string $ciudad
     *
     * @return Superpersona
     */
    public function setCiudad($ciudad) {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad
     *
     * @return string
     */
    public function getCiudad() {
        return $this->ciudad;
    }

    /**
     * Set fuerza
     *
     * @param integer $fuerza
     *
     * @return Superpersona
     */
    public function setFuerza($fuerza) {
        $this->fuerza = $fuerza;

        return $this;
    }

    /**
     * Get fuerza
     *
     * @return integer
     */
    public function getFuerza() {
        return $this->fuerza;
    }

    /**
     * Set inteligencia
     *
     * @param integer $inteligencia
     *
     * @return Superpersona
     */
    public function setInteligencia($inteligencia) {
        $this->inteligencia = $inteligencia;

        return $this;
    }

    /**
     * Get inteligencia
     *
     * @return integer
     */
    public function getInteligencia() {
        return $this->inteligencia;
    }

    /**
     * Set salud
     *
     * @param integer $salud
     *
     * @return Superpersona
     */
    public function setSalud($salud) {
        $this->salud = $salud;

        return $this;
    }

    /**
     * Get salud
     *
     * @return integer
     */
    public function getSalud() {
        return $this->salud;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Superpersona
     */
    public function setTipo($tipo) {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo() {
        return $this->tipo;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Superpersona
     */
    public function setActivo($activo) {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo() {
        return $this->activo;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Superpersona
     */
    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return Superpersona
     */
    public function setFechaModificacion($fechaModificacion) {

        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    /**
     * Set idusuario
     *
     * @param integer $idusuario
     *
     * @return Superpersona
     */
    public function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;

        return $this;
    }

    /**
     * Get idusuario
     *
     * @return integer
     */
    public function getIdusuario() {
        return $this->idusuario;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Superpersona
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

}
