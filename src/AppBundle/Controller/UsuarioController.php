<?php


namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Usuario;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use AppBundle\Form\RegistroUsuarioType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use Symfony\Component\HttpFoundation\JsonResponse;

class UsuarioController extends Controller {
    
    
    public function loginAction(Request $request){
        $authenticationUtils = $this->get("security.authentication_utils");
        $form_new_usuario = $this->crearRegistroForm();
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        
        
        if($error != null){
            $this->addFlash(
            'status',
            'La contraseña o el email no ha sido insertada correctamente. Por favor vuelve a intertarlo nievamente'
        );
        }
        
        
  
        return $this->render("@App/Usuario/login.html.twig",array(
           "error" => $error,
            "lastUsername" => $lastUsername,
            "form" => $form_new_usuario->createView(),
            "login" => true
        ));
    }
    
    private function crearRegistroForm(){
        return $this->createForm(RegistroUsuarioType::class,new Usuario(),array(
            'action' => $this->generateUrl('creacion_usuario'),
            'method' => 'POST'
        ));
    }
    
    public function crearUsuarioAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $form = $this->crearRegistroForm();
        $form->handleRequest($request);
        
        if($form->isValid()){
            $usuario = $form->getData();
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($usuario,$usuario->getPassword());
            $usuario->setPassword($encoded);
            $usuario->setRol('ROLE_ADMIN');
            $usuario_repo = $em->getRepository("AppBundle:Usuario");
            $busquedaEmail = $usuario_repo->findOneByEmail($usuario->getEmail());
            if(count($busquedaEmail) > 0){
                $mensaje = "El email que has introducido ya está registrado";
                $estado = "error";
                return $this->render("@App/Usuario/login.html.twig",array(
                "form" => $form->createView(),
                "mensajeEmailError" => $mensaje,
                "activarTagRegistro" => 1,
                "login" => true
                ));
            }
            else{
                $em->persist($usuario);
                $em->flush();
                $token = new UsernamePasswordToken($usuario, null, 'default', $usuario->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main',serialize($token));
                return $this->redirectToRoute("app_homepage");
            }
        }
         return $this->render("@App/Usuario/login.html.twig",array(
            "form" => $form->createView(),
             "activarTagRegistro" => 1,
             "login" => true
        ));
        
    }
    
    public function loginCheckAction(){
        
    }
     
    

}
