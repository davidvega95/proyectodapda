<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use AppBundle\Form\SuperpersonaType;
use AppBundle\Entity\Superpersona;
use Symfony\Component\Validator\Constraints\DateTime;

class ApiRestSuperpersonaController extends Controller {

    public function indexAction() {

        return $this->render('@App/ApiRestSuperpersona/index.html.twig', array(
                    'activo' => 1
        ));
    }

    public function listaNoActivosAction() {

        return $this->render('@App/ApiRestSuperpersona/index.html.twig', array(
                    'activo' => 0
        ));
    }

    // funcion donde me saca el listado de superpoderes
    public function readAction(Request $request) {

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();
        $superpersonas = $em->getRepository('AppBundle:Superpersona')->findByActivo('1');
        $response = new JsonResponse();
        $response->setStatusCode(200);
        $response->setData(array(
            'response' => 'success',
            'superpersonas' => $serializer->serialize($superpersonas, 'json')
        ));

        return $response;
    }

    public function readNoActivosAction(Request $request) {

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $em = $this->getDoctrine()->getManager();
        $superpersonas = $em->getRepository('AppBundle:Superpersona')->findByActivo('0');
        $response = new JsonResponse();
        $response->setStatusCode(200);
        $response->setData(array(
            'response' => 'success',
            'superpersonas' => $serializer->serialize($superpersonas, 'json')
        ));

        return $response;
    }

    // funcion donde me saca el listado de superpoderes
    public function deleteAction(Request $request, $id) {
        //$data = $request->query->get('id');
        $message = "";

        $em = $this->getDoctrine()->getManager();
        $superpersona_repo = $em->getRepository("AppBundle:Superpersona");
        $superpersona = $superpersona_repo->findOneByIdSuperpersona($id);
        $superpersona->setActivo('0');
        $flush = $em->flush();
        $response = new JsonResponse();
        $response->setStatusCode(200);
        $response->setData(array(
            'response' => 'success',
            'posts' => $message
        ));

        return $response;
    }

    public function modificarFormAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $superpersona_repo = $em->getRepository("AppBundle:Superpersona");
        $superpersona = $superpersona_repo->findOneByIdSuperpersona($id);
        $form = $this->createForm(SuperpersonaType::class, $superpersona, array(
            'attr' => array(
                'id' => 'formulario',
                'class' => 'form'
            )
        ));

        $form->handleRequest($request);

        return $this->render("@App/ApiRestSuperpersona/FormSuperpersona.html.twig", array(
                    "superpersona" => $superpersona,
                    'form' => $form->createView(),
                    'id' => $id,
                    'titulo' => "Modificar Superpersona",
                    'url' => 'put'
        ));
    }

    public function crearAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $superpersona_repo = $em->getRepository("AppBundle:Superpersona");
        $superpersona = new Superpersona();
        //$form = $this->createForm(SuperpersonaType::class, $superpersona);
        $form = $this->createForm(SuperpersonaType::class, $superpersona, array(
            'attr' => array(
                'id' => 'formulario',
                'class' => 'form'
            )
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $superpersona = new Superpersona();
                $superpersona->setNombre($form->get("nombre")->getData());
                $superpersona->setCiudad($form->get("ciudad")->getData());
                $superpersona->setFuerza($form->get("fuerza")->getData());
                $superpersona->setInteligencia($form->get("inteligencia")->getData());
                $superpersona->setSalud($form->get("salud")->getData());
                $superpersona->setTipo($form->get("tipo")->getData());
                $superpersona->setIdusuario(1);
                $superpersona->setSlug("create");



                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($superpersona);
                $flush = $em->flush();
                if ($flush == null) {
                    $status = "La superpersona se ha creado correctamente";
                    $this->addFlash(
                            'noticeCreateCorrecto', $status
                    );
                } else {
                    $status = "La superpersona no se ha creado correctamente";
                    $this->addFlash(
                            'noticeErrorCreate', $status
                    );
                }
            } else {
                $status = "No te has registrado correctamente";
            }
        }
        return $this->render("@App/ApiRestSuperpersona/FormSuperpersona.html.twig", array(
                    "superpersona" => $superpersona,
                    'form' => $form->createView(),
                    'titulo' => "Añadir Superpersona",
                    'url' => "post"
        ));
    }

    public function crearPostAction(Request $request) {

        //$data = $request->query->get('id');
        $message = "";
        $em = $this->getDoctrine()->getManager();
        //$request = $this->get('request');
        //$ev = $request->request->get('ev');
        $nombre = $request->get("nombre");
        $ciudad = $request->get("ciudad");
        $tipo = $request->get("tipo");
        $fuerza = $request->get("fuerza");
        $inteligencia = $request->get("inteligencia");
        $salud = $request->get("salud");

        $superpersona_repo = $em->getRepository("AppBundle:Superpersona");
        $busquedaNombre = $superpersona_repo->findOneByNombre($nombre);
        if (count($busquedaNombre) > 0) {
            $mensaje = "El nombre que has introducido está repetido";
            $estado = "error";
            $encoders = array(new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());

            $serializer = new Serializer($normalizers, $encoders);
        } else {
            $superpersona = new Superpersona();

            $superpersona->setNombre($nombre);
            $superpersona->setCiudad($ciudad);
            $superpersona->setFuerza($fuerza);
            $superpersona->setInteligencia($inteligencia);
            $superpersona->setSalud($salud);
            $superpersona->setTipo($tipo);
            $superpersona->setIdusuario(1);
            $superpersona->setSlug("create");

            $encoders = array(new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());

            $serializer = new Serializer($normalizers, $encoders);
            $em->persist($superpersona);
            $flush = $em->flush();

            if ($flush == null) {
                $mensaje = "La superpersona se ha creado correctamente";
                $estado = "success";
            } else {
                $mensaje = "La superpersona no se ha creado correctamente";
                $estado = "error";
            }
        }

        $response = new JsonResponse();
        $response->setStatusCode(200);

        $response->setData(array(
            'response' => $estado,
            'mensaje' => $mensaje,
            'posts' => $serializer->serialize($nombre, 'json')
        ));

        return $response;
    }

    public function crearPutAction(Request $request) {

        //$data = $request->query->get('id');

        $em = $this->getDoctrine()->getManager();
        $superpersona_repo = $em->getRepository("AppBundle:Superpersona");
        $id = $request->request->get("id");
        $superpersona = $superpersona_repo->findOneByIdSuperpersona($id);
        $nombre = $request->get("nombre");
        $busquedaNombre = $superpersona_repo->findOneByNombre($nombre);
        if ($superpersona->getNombre() === $nombre || count($busquedaNombre) === 0) {

            $ciudad = $request->get("ciudad");
            $tipo = $request->get("tipo");
            $fuerza = $request->get("fuerza");
            $inteligencia = $request->get("inteligencia");
            $salud = $request->get("salud");
            $superpersona->setNombre($nombre);
            $superpersona->setCiudad($ciudad);
            $superpersona->setFuerza($fuerza);
            $superpersona->setInteligencia($inteligencia);
            $superpersona->setSalud($salud);
            $superpersona->setTipo($tipo);
            $fechaActual = new \DateTime('now');

            $superpersona->setFechaModificacion($fechaActual);


            $em->persist($superpersona);
            $flush = $em->flush();
            if ($flush == null) {
                $mensaje = "La superpersona se ha modificado correctamente";
                $estado = "success";
            } else {
                $mensaje = "La superpersona no se ha modificado correctamente";
                $estado = "error";
            }
            $encoders = array(new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());

            $serializer = new Serializer($normalizers, $encoders);
        } else {
            $mensaje = "El nombre a modificar ya existe";
            $estado = "error";
            $encoders = array(new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());

            $serializer = new Serializer($normalizers, $encoders);
        }
        $response = new JsonResponse();
        $response->setStatusCode(200);

        $response->setData(array(
            'response' => $estado,
            'mensaje' => $mensaje,
        ));

        return $response;
    }

    public function recuperarDatosAction(Request $request) {
        //$data=$request->query->get('data');
        $data = $request->get('datosSeleccionado');
        $mensaje = "";
        $estado = "";
        $em = $this->getDoctrine()->getManager();
        $superpersona_repo = $em->getRepository("AppBundle:Superpersona");
        for ($i = 0; $i < count($data); $i++) {
            try {
                $superpersona = $superpersona_repo->findOneByIdSuperpersona($data[$i]);
                $superpersona->setActivo('1');
                $em->persist($superpersona);
                $flush = $em->flush();
            } catch (Exception $e) {
                $mensaje = "Error a recuperar dato ";
                $estado = "error";
            }
        }
        if ($mensaje == "") {
            $mensaje = "Se ha podido recuperar todos los datos seleccionados";
            $estado = "success";
        }
        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        $response = new JsonResponse();
        $response->setStatusCode(200);

        $response->setData(array(
            'response' => $estado,
            'mensaje' => $mensaje
        ));

        return $response;
    }

}
