<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;

class SuperpersonaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('nombre',TextType::class, array("label"=>"Nombre:","required"=>"required",
                'label_attr' => array('class' => 'col-lg-4 control-label'),"attr"=>array(
                "class"=>"form-control message-input", "aria-required" =>true)))
                ->add('ciudad',TextType::class, array("label"=>"Ciudad:","required"=>"required",
                'label_attr' => array('class' => 'col-lg-4 control-label'),"attr"=>array(
                "class"=>"form-control"))) 
                ->add('tipo', ChoiceType::class,array(
                "required"=>"required",
                    "attr"=>array(
                "class"=>"form-control"),
                'label_attr' => array('class' => 'col-lg-4 control-label'),
                'choices' => array(
                    'Seleccione..' => "",
                    'Superhéroe' => 1, 
                    'Villano' => 2)
                ))   
                ->add('fuerza',RangeType::class, array("label"=>"Fuerza","required"=>"required",'attr' => array(
                'min' => 0,
                'max' => 100,
                    "class"=>"form-control"),
                 'label_attr' => array('class' => 'col-lg-4 control-label')))
                ->add('inteligencia',RangeType::class, array("label"=>"Inteligencia","required"=>"required","attr"=>array(
                'min' => 0,
                'max' => 100,
                "class"=>"form-control"),
                'label_attr' => array('class' => 'col-lg-4 control-label')))
                ->add('salud',RangeType::class, array("label"=>"Salud","required"=>"required","attr"=>array(
                'min' => 0,
                'max' => 100,
                  "class"=>"form-control",   
                 "value" => 0),
                    'label_attr' => array('class' => 'col-lg-4 control-label')));
        /*        ->add('save', SubmitType::class, array(
                'attr' => array('class' => 'save')));
          */      
               
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Superpersona'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_superpersona';
    }


}
